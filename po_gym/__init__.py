from gym.envs.registration import register

register(
    id='tiger-v0',
    entry_point='po_gym.envs:Tiger',
    timestep_limit=1000,
    trials=10000
)
#register(
#    id='foo-extrahard-v0',
#    entry_point='gym_foo.envs:FooExtraHardEnv',
#    timestep_limit=1000,
#)
