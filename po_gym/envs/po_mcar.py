import gym
import numpy as np
import sys
from six import StringIO
from gym.envs.classic_control import MountainCarEnv
from gym import error, spaces, utils
from gym.utils import seeding
import pdb

class POMountainCar(MountainCarEnv):
    def __init__(self):
        n_states = 10
        self.subsample = 7
        self.sspace = np.linspace(0, 1, n_states + 1)

        super(POMountainCar, self).__init__()

        self.observation_space = spaces.Discrete(n_states)

    def _quantize(self):
        o = np.digitize((self.state[0] - self.low[0])/(self.high[0] - self.low[0]), self.sspace) -1  # only position is used
        return o

    def _step(self, action):
        done = False
        for _ in xrange(self.subsample):
            s, r, d, p = super(POMountainCar, self)._step(action)
            done = d or done
        o = self._quantize()
        if done:
            r = 100
        return o, r, done, p

    def _reset(self):
        super(POMountainCar, self)._reset()
        return self._quantize()
