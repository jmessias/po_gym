import gym
import numpy as np
import sys
from six import StringIO
from gym import spaces
from gym.utils import seeding


class Tiger(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        self.action_space = spaces.Discrete(3)  # listen, open left, open right
        self.observation_space = spaces.Discrete(2)  # tiger left, tiger right

        self._seed()
        self._reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset(self):
        self.s = int(np.random.rand() < 0.5)
        o = int(np.random.rand() < 0.5)
        return o

    def _step(self, a):
        if a == 0:  # listen
            r = -1
            true_obs = np.random.rand() < 0.85
            if true_obs:
                o = self.s
                p = 0.85
            else:
                o = 1 - self.s
                p = 0.15
            done = False
        else:
            r = -100 if (self.s + 1 == a) else 10
            o = int(np.random.rand() < 0.5)
            p = 0.5
            done = True

        return (o, r, done, {"prob" : p})

    def _render(self, mode='human', close=False):
        outfile = StringIO() if mode == 'ansi' else sys.stdout
        state = "Left" if self.s else "Right"
        outfile.write("State: %s\n" % state)
        return outfile
