import gym
import numpy as np
import sys
from six import StringIO
from gym import error, spaces, utils
from gym.utils import seeding


class NoisyTMaze(gym.Env):
    metadata = {'render.modes': ['human']}
    success_rwd = 100
    failure_rwd = -5
    move_rwd = -1
    bump_rwd = -5

    def __init__(self, length=4, uninformative_observations=1):
        self.nA = 4  # N, E, S, W
        self.nO = 3 + uninformative_observations  # switch state: Up, Down + At T junction
        self.length = length

        self.action_space = spaces.Discrete(4)
        self.observation_space = spaces.Discrete(self.nO)

        self._cb_map = [self._move_north,
                        self._move_east,
                        self._move_south,
                        self._move_west]
        self._seed()
        self._reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset(self):
        self.switch_state = int(np.random.rand() < 0.5)  # 0 is down, 1 is up
        self.robot_state = 0

        o = self.switch_state
        return o

    def _move_west(self):
        if self.robot_state > 0:
            self.robot_state -= 1
        r = NoisyTMaze.move_rwd
        done = False
        o = self._get_observation()
        return (o, r, done)

    def _move_east(self):
        if self.robot_state < self.length - 1:
            self.robot_state += 1
        r = NoisyTMaze.move_rwd
        done = False
        o = self._get_observation()
        return (o, r, done)

    def _move_north(self):
        if self.robot_state != self.length - 1:
            r = NoisyTMaze.bump_rwd
            done = False
        else:
            if self.switch_state == 1:
                r = NoisyTMaze.success_rwd
            else:
                r = NoisyTMaze.failure_rwd
            done = True
            self._reset()
        o = self._get_observation()
        return (o, r, done)

    def _move_south(self):
        if self.robot_state != self.length - 1:
            r = NoisyTMaze.bump_rwd
            done = False
        else:
            if self.switch_state == 1:
                r = NoisyTMaze.failure_rwd
            else:
                r = NoisyTMaze.success_rwd
            done = True
            self._reset()
        o = self._get_observation()
        return (o, r, done)

    def _get_observation(self):
        if self.robot_state == 0:
            o = self.switch_state
        elif self.robot_state == self.length - 1:
            o = 2  # T-junction
        else:
            o = 3 + np.random.choice(self.nO - 3)
        return o

    def _step(self, a):
        (o, r, done)  = self._cb_map[a]()
        return (o, r, done, {"prob" : 1.0})
