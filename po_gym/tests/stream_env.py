#!/usr/bin/env python
import gym
import logging
import time
import socket
import yaml
import numpy as np
from collections import deque
from argparse import ArgumentParser
from po_gym.utils.stream_io import StreamServer
from po_gym.envs.noisy_tmaze import NoisyTMaze
from po_gym.envs.tiger import Tiger
from po_gym.envs.po_mcar import POMountainCar
import os

po_envs = {'NoisyTMaze-v0': NoisyTMaze,
           'Tiger-v0': Tiger,
           'POMountainCar-v0': POMountainCar}


def main():
    parser = ArgumentParser(description='Run a gym environment and transmit actions / observations / rewards via as'
                                        'bitstrings through a socket.')
    parser.add_argument('env', metavar='ENV',
                        help='The ID of the gym environment to load')
    parser.add_argument('-e', '--env-params',
                        type=str,
                        dest='env_params',
                        default='env_params.yaml',
                        help="Path to a YAML file containing environment parameters. Default is 'env_params.yaml'")
    parser.add_argument('-H', '--host',
                        type=str,
                        dest='host',
                        default='localhost',
                        help='The hostname to use in the stream server. Default is localhost.')
    parser.add_argument('-p', '--port',
                        type=int,
                        dest='port',
                        default=8096,
                        help='The port to use for communication with the agent. Default is 8096')
    parser.add_argument('-T', '--timeout',
                        type=float,
                        dest='timeout',
                        default=5.0,
                        help='The timeout to use during socket read/write operations, in seconds. '
                             'Default is 5.0 seconds.')
    parser.add_argument('--render',
                        action='store_true',
                        dest='render',
                        help='Turn on environment rendering.')
    parser.add_argument('-v', '--rec-video',
                        action='store_true',
                        dest='rec_video',
                        help='Turn on video recording.')
    parser.add_argument('-o', '--output-dir',
                        type=str,
                        dest='output_dir',
                        default='/tmp/gym-results',
                        help='Base directory in which to store the gym monitor results / videos. '
                             'Default is /tmp/gym-results')
    parser.add_argument('-u', '--uuid',
                        type=str,
                        dest='uuid',
                        default=None,
                        help='An unique identifier for this experiment. Defaults to the current time.')

    
    args = parser.parse_args()
    datestr = time.strftime('%d_%b_%Y_%H.%M.%S', time.gmtime())
    suffix = args.uuid if args.uuid is not None else datestr
    assert len(args.output_dir) > 0
    if args.output_dir[-1] != '/':
        args.output_dir += '/'
    of_dir = args.output_dir + suffix
    
    """ Simulation """
    try:
        env = gym.make(args.env)
    except gym.error.UnregisteredEnv:
        env = po_envs[args.env]()
    rec_video = None if args.rec_video else False
    env = gym.wrappers.Monitor(env, of_dir, video_callable=rec_video)

    assert (type(env.action_space) == gym.spaces.discrete.Discrete and
            type(env.observation_space) == gym.spaces.discrete.Discrete), \
        'socket_tester requires discrete action and observation spaces.'

    with open(args.env_params) as env_params_file:
        env_params_dict = yaml.load(env_params_file)
    env_params = env_params_dict[args.env]

    n_actions = env.action_space.n
    n_observations = env.observation_space.n
    min_r = min(env_params['reward_space'])
    delta_r = max(env_params['reward_space']) - min_r
    R_hist = deque([], maxlen=100)

    server = StreamServer(n_actions, n_observations, delta_r, args.host, timeout=args.timeout, port=args.port)

    finished = False
    while not finished:
        try:
            for e in xrange(int(float(env_params['number_of_episodes']))):  # 1eX is not parsed by int, but it needs to be an int for xrange
                r = 0            # J. Veness' domains assume that an initial reward is sent along with the initial observation
                total_r = 0
                o = env.reset()  # returns initial observation
                for t in xrange(env_params['timestep_limit']):
                    server.send_env_info(o, r)
                    a = server.recv_action()

                    (o, r, done, info) = env.step(a)
                    total_r += r
                    r -= min_r  # reward is shifted to positive

                    if args.render:
                        env.render()
                        print 'Last action: %d' % a
                        print 'Observation: %d' % o
                        print 'Reward: %.2f' % (r + min_r)
                        print '=== Step %d complete ===' % t
                    if done:
                        server.send_env_info(o, r)  # necessary for the agent to know the final observation
                        server.recv_action()  # final action, not used
                        if args.render:
                            print '=== Terminal state reached === '
                        break

                print '=== Episode %d complete ===' % e
                R_hist.append(total_r)
                print '=== Mean reward so far (past 100 eps.): %.2f' % np.mean(R_hist)
            finished = True
        except socket.error:
            logging.error('Connection with agent lost. Resetting environment.')
            server.wait_for_client()

if __name__ == '__main__':
    main()
