import socket
import time
import numpy as np
import logging
import pdb

class StreamInterface(object):
    @staticmethod
    def send_int(socket_iface, num, nbits):
        if socket_iface is None:
            raise socket.error("StreamInterface -- Not connected!")
        if not int(num) == num:
            raise ValueError("StreamInterface only supports discrete values. "
                             "Conversion to int would discard information.")
        bitstr = bin(int(num))[2:]  # remove "0b" prefix
        dlen = nbits - len(bitstr)
        if dlen > 0:
            bitstr = "0" * dlen + bitstr
        elif dlen < 0:
            raise ValueError("StreamInterface: send op requested for bitstring of length %d but num requires at least"
                             " %d bits to represent!" % (nbits, len(bitstr)))
        nsent = 0
        while nsent < nbits:
            nsent += socket_iface.send(bitstr[nsent:])

    @staticmethod
    def recv_int(socket_iface, nbits, timeout=1.0):
        if socket_iface is None:
            raise socket.error("StreamInterface -- Not connected!")
        recv_data = ""
        t0 = time.time()
        while len(recv_data) < nbits:
            d = socket_iface.recv(nbits - len(recv_data))
            t1 = time.time()
            if len(d) > 0:
                t0 = t1
            if t1 - t0 > timeout:
                raise socket.error("Did not receive data for %.2f seconds." % timeout)
            recv_data += d
            break_pos = recv_data.rfind('\n')
            if break_pos > -1:
                try:
                    recv_data = recv_data[break_pos+1:]
                except IndexError:
                    recv_data = ""
        return int(recv_data[-nbits:], 2)


class StreamServer(StreamInterface):
    def __init__(self, n_actions, n_observations, delta_r, host='localhost', port=8096, timeout=1.0):
        self.socket = None
        self.conn = None
        self.timeout = timeout
        self.a_bits = int(np.ceil(np.log2(n_actions)))
        self.o_bits = int(np.ceil(np.log2(n_observations)))
        self.r_bits = int(np.floor(np.log2(delta_r)) + 1)
        #logging.warn('Initializing StreamServer with parameters:\n_actionsction bits: %d\nobservation bits: %d\n'
        #             'reward bits: %d' % (self.a_bits, self.o_bits, self.r_bits))
        self.wait_for_client(host, port)

    def wait_for_client(self, host, port):
        socket_valid = False
        while not socket_valid:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.settimeout(self.timeout)
                self.socket.bind((host, port))
                self.socket.listen(1)
                socket_valid = True
            except socket.error as e:
                print e
                time.sleep(self.timeout)
        connected = False
        while not connected:
            try:
                print 'StreamServer waiting for connection on port %d' % port
                self.conn, addr = self.socket.accept()
                connected = True
                print 'Connection established to ', addr
            except socket.timeout:
                pass

    def send_break(self):
        self.conn.send('\n')

    def send_observation(self, o):
        self.send_int(self.conn, o, self.o_bits)

    def send_reward(self, r):
        self.send_int(self.conn, r, self.r_bits)

    def recv_action(self):
        return self.recv_int(self.conn, self.a_bits, self.timeout)

    def send_env_info(self, observation, reward):
        self.send_observation(observation)
        self.send_reward(reward)
        self.send_break()


class StreamClient(StreamInterface):
    def __init__(self, a_bits, o_bits, r_bits, host='localhost', port=8096, timeout=1.0):
        self.socket = None
        self.timeout = timeout
        self.conn = None
        self.a_bits = a_bits
        self.o_bits = o_bits
        self.r_bits = r_bits
        logging.warn('Initializing StreamClient with parameters:\naction bits: %d\nobservation bits: %d\n'
                     'reward bits: %d' % (self.a_bits, self.o_bits, self.r_bits))
        self.connect_to_server(host, port)

    def connect_to_server(self, host, port):
        socket_valid = False
        while not socket_valid:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.settimeout(self.timeout)
                self.socket.connect((host, port))
                socket_valid = True
            except socket.error as e:
                print e
                print 'Retrying in %.1f seconds...' % self.timeout
                time.sleep(self.timeout)
        print 'Connection established to', host

    def send_break(self):
        self.socket.send('\n')

    def send_action(self, a):
        self.send_int(self.socket, a, self.a_bits)
        self.send_break()

    def recv_observation(self):
        return self.recv_int(self.socket, self.o_bits)

    def recv_reward(self):
        return self.recv_int(self.socket, self.r_bits)

    def recv_env_info(self):
        o = self.recv_observation()
        r = self.recv_reward()
        return (o, r)
